#include "pomodoromanagerview.h"
#include "ui_form.h"

PomodoroManagerView::PomodoroManagerView() :
	QObject(),
	PluginBase(this),
	ui(new Ui::Form),
	m_GUIElementBase(new GUIElementBase(this, {"MainMenuItem"}))
{
	ui->setupUi(m_GUIElementBase);

	connect(ui->treeView, SIGNAL(pressed(const QModelIndex &)), this, SLOT(treeView_pressed(const QModelIndex &)));
	connect(ui->buttonExit, SIGNAL(clicked(bool)), SLOT(buttonExit_clicked()));

	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_GUIElementBase}
	},
	{
		{INTERFACE(IPomodoroManager), myModel}
	});
	m_GUIElementBase->initGUIElementBase();
}

PomodoroManagerView::~PomodoroManagerView()
{
}

QString PomodoroManagerView::linkName()
{
	return m_GUIElementBase->linkNames().first();
}

void PomodoroManagerView::onReferencesSet()
{
	connect(ui->pomodoroButton, SIGNAL(buttonPressed()), SLOT(pomodoroButtonPressed()));

	connect(myModel.reference()->object(), SIGNAL(onStatusChanged(TimerStatus, QTime)), SLOT(onStatusChanged(TimerStatus, QTime)));
	connect(myModel.reference()->object(), SIGNAL(onTimerTick(QTime)), SLOT(onTimerTick(QTime)));
}

void PomodoroManagerView::onReady()
{
	proxyModel = myModel->GetTaskModel();
	proxyModelFilter = myModel->GetTaskModelFilter();
	ui->treeView->setModel(proxyModelFilter);
	//	ui->treeView->header()->setSectionResizeMode(0, QHeaderView::ResizeMode::Stretch);
	UpdateSelectedTask();
}

void PomodoroManagerView::pomodoroButtonPressed()
{
	if(!ui->pomodoroButton->isEnabled())
	{
		return;
	}

	IPomodoroManager::TimerStatus newState;
	switch (myModel->getStatus()) {
	case IPomodoroManager::TimerStatus::PAUSE:
		newState = IPomodoroManager::TimerStatus::WORK;
		break;
	case IPomodoroManager::TimerStatus::REST:
		newState = IPomodoroManager::TimerStatus::PAUSE;
		break;
	case IPomodoroManager::TimerStatus::WORK:
		newState = IPomodoroManager::TimerStatus::PAUSE;
		break;
	}
	myModel->changeStatus(newState);
}

void PomodoroManagerView::onStatusChanged(TimerStatus status, QTime newTime)
{
	QString stateStr;
	switch (status) {
	case IPomodoroManager::TimerStatus::PAUSE:
		stateStr = "Start Work";
		break;
	case IPomodoroManager::TimerStatus::REST:
		stateStr = "Stop Rest";
		break;
	case IPomodoroManager::TimerStatus::WORK:
		stateStr = "Stop Work";
		break;
	}
	UpdateSelectedTask();
	ui->pomodoroButton->setNewTime(newTime.msecsSinceStartOfDay());
	ui->pomodoroStateLabel->setText(stateStr);
	ui->pomodoroTimeLeft->setText(newTime.toString());
}

void PomodoroManagerView::onTimerTick(QTime timeLeft)
{
	ui->pomodoroButton->timerTick(timeLeft.msecsSinceStartOfDay());
	ui->pomodoroTimeLeft->setText(timeLeft.toString());
}

void PomodoroManagerView::buttonExit_clicked()
{
	m_GUIElementBase->closeSelf();
}

void PomodoroManagerView::UpdateSelectedTask()
{
	bool isTaskSelected = currentTask.isValid();
	ui->pomodoroButton->setEnabled(isTaskSelected);
	auto project = myModel->getActiveProjectPomodoros();
	ui->pomodoroStateLabel->setText(isTaskSelected ? "Start Work" : "");
	ui->pomodoroTimeLeft->setText("");
	ui->labelProject->setText(isTaskSelected ? QString("Task: %1").arg(project.first) : "Task not selected");
	ui->pomodoroCountLabel->setText(isTaskSelected ? QString("Pomodoros: %2").arg(project.second) : "");
	ui->treeView->update();
}

void PomodoroManagerView::treeView_pressed(const QModelIndex &index)
{
	auto list = ui->treeView->selectionModel()->selectedIndexes();
	if(list.length() == 0) return;
	auto selected = list.first();

	currentTask = proxyModelFilter->mapToSource(selected);
	myModel->SetActiveProject(currentTask);

	UpdateSelectedTask();
}
